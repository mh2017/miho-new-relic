<?php

namespace MihoNewRelicApm;

use Shopware\Components\Plugin;

class MihoNewRelicApm extends Plugin
{

	public function getInfo()
	{
		return array(
			'description' => 'Benennt die häufigsten +15 Transaktionen in New Relic um',
		);
	}


	/**
	 * @inheritdoc
	 */
	public static function getSubscribedEvents()
	{
		return ['Enlight_Controller_Action_PostDispatch' => 'onPostDispatch'];
	}


	public function onPostDispatch(\Enlight_Controller_ActionEventArgs $args)
	{
		$request = $args->getSubject()->Request();
		$response = $args->getSubject()->Response();

		if (!$request->isDispatched() || $response->isException()) {
			return;
		}

		if (extension_loaded('newrelic')) {

			$transaction_build = $request->getControllerName() . '/' . $request->getActionName();

			//This switch will simply rename the transactions
			switch($transaction_build)
			{
				case "listing/index":
					$transaction = 'Kategorieseite';
					break;

				case "index/index":
					$transaction = 'Startseite';
					break;

				case "blog/index":
					$transaction = 'Blogübersicht';
					break;

				case "wishlist/index":
					$transaction = 'Wunschliste';
					break;

				case "blog/detail":
					$transaction = 'Blogdetailseite';
					break;

				case "detail/index":
					$transaction = 'Produktdetailseite';
					break;

				case "RecommendationSlider/detail":
					$transaction = 'Produktdetailseitenslider';
					break;

				case "search/defaultSearch":
					$transaction = 'Standard Suche';
					break;

				case "ajax_search/index":
					$transaction = 'Ajax Suche';
					break;

				case "register/index":
					$transaction = 'Registrierung';
					break;

				case "account/index":
					$transaction = 'Account';
					break;

				case "checkout/cart":
					$transaction = 'Warenkorb';
					break;

				case "checkout/confirm":
					$transaction = 'Bestellbestätigung';
					break;

				case "checkout/ajax_add_article":
					$transaction = 'Artikel zum WK hinzufügen';
					break;

				case "checkout/finish":
					$transaction = 'Bestellabschluss';
					break;

				case "checkout/shippingPayment":
					$transaction = 'Zahlungsartänderung (Checkout)';
					break;

				case "checkout/info":
					$transaction = 'Warenkorb Call im Header';
					break;

				case "index/shopMenu":
					$transaction = 'Shopmenu';
					break;

				case "forms/index":
					$transaction = 'Formular';
					break;

				case "custom/index":
					$transaction = 'Shopseite';
					break;

				case "listing/manufacturer":
					$transaction = 'Herstellerseite';
					break;

				case "detail/error":
					$transaction = 'Artikel nicht gefunden';
					break;

				case "RecommendationSlider/productStreamSliderRecommendations":
					$transaction = 'Empfehlungsslider';
					break;


				default:
					$transaction = $transaction_build;
					break;
			}

			//Set the "new relic" transaction name - https://docs.newrelic.com/docs/agents/php-agent/php-agent-api/newrelic_name_transaction
			newrelic_name_transaction($transaction);
		}

	}

}